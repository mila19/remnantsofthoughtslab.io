---
title: "Über mich"
date: 2020-07-13T11:48:27+02:00
draft: true
---

Das Leben ist hart und kompliziert und nicht immer, wie man es haben möchte, aber man sollte trotz allem weiter immer gehen. Irgendwann wird es klappen.
Hier werde ich meine Gedanken, Erfahrungen, meine Kreation und was weiß ich noch mit anderen teilen. Ich bin eine Person, die versucht etwas in ihrem Leben zu erreichen und gleichzeitig etwas Spannendes (meiner Ansicht nach) zu erleben. Aber das weiß ja niemand, ob das stimmt.
Seid nett immer alle zu einander.

 
